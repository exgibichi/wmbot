<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use \app\models\Config;
use \app\models\Orders;
use \app\models\Events;
use \app\models\Stats;
use \app\models\TestForm;
use \app\models\SessionsInfo;
use \app\TradeLib\Bot;
use yii\filters\AccessControl;
use yii\data\ActiveDataProvider;


class SiteController extends Controller {

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
                'view' => 'error.twig'
            ],
        ];
    }

    public function init() {
        $config = Config::loadConfig();
        $this->view->params['active_bids'] = Orders::find()->where(['not', 'status', 'finished'])->count();
        $this->view->params['sound_alert'] = $config ? $config->sound : 0;
        $session = SessionsInfo::find()->one();
        if($session) {
            if(isset($session->wmz)) {
                $this->view->params['wmz'] = $session->wmz;
            } else {
                $this->view->params['wmz'] = '-';
            }
            if(isset($session->wmr)) {
                $this->view->params['wmr'] = $session->wmr;
            } else {
                $this->view->params['wmr'] = '-';
            }
        }
        return parent::init();
    }

    public function actionGetEvents() {
        $events = Events::getNew();
        return json_encode($events);
    }

    public function actionChangeDiff($bid_id, $ref) {
        $order = Orders::find()->where(['bid_id' => $bid_id])->andWhere(['not', 'status', 'finished'])->one();
        if(\Yii::$app->request->post()) {
            $data = \Yii::$app->request->post('Orders');
            $status = Orders::saveDiff($bid_id, $data['diff'], 'diff', 1);
            if($status) {
                \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Дифф изменен']);
                return $this->redirect(['site/'.$ref]);
            } else {
                \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
            }
        }
        return $this->render('diff.twig', ['order' => $order]);
    }

    public function actionIndex() {
        $config = Config::loadConfig();
        $orders = Orders::find()->where(['not', 'status', 'finished'])->all();
        return $this->render('index.twig', [
            'config' => $config,
            'orders' => $orders,
        ]);
    }

    public function actionHistoryDetail($bid_id) {
        $order = Orders::find()->where(['bid_id' => $bid_id])->one();
        if(!$order) {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка, нет такой заявки']);
            return $this->redirect(['site/history']);
        }
        $history = Orders::compileHistory($order->history);
        return $this->render('detail.twig', [
            'order' => $order,
            'history' => $history,
        ]);
    }

    public function actionHistory() {
        Events::cancel('bid');
        $query = Orders::find()->where(['status' => 'finished'])->orderBy('time DESC');
        $countQuery = clone $query;
        $cnt = $countQuery->count();
        $pages = new \yii\data\Pagination(['totalCount' => $cnt]);
        $orders = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
        $live_orders = Orders::find()->where(['not', 'status', 'finished'])->all();
        return $this->render('history.twig', ['live_orders' => $live_orders, 'orders' => $orders, 'pages' => $pages]);
    }

    public function actionChangeType($bid_id, $ref) {
        $order = Orders::find()->where(['bid_id' => $bid_id])->select(['type'])->one();
        if(!$order) {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка, нет такой заявки']);
            return $this->redirect(['site/'.$ref]);
        }
        $new_type = $order->type == 'handmade' ? 'botmade' : 'handmade';
        $status = Orders::saveDiff($bid_id, $new_type, 'type', 1);
        if($status) {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Тип заявки изменен успешно']);
        } else {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
        }
        return $this->redirect(['site/'.$ref]);
    }

    public function actionCancelBid($bid_id, $ref) {
        $order = Orders::find()
        ->where(['bid_id' => $bid_id])
        ->andWhere(['not', 'status', 'finished'])
        ->select(['bid_id'])
        ->one();
        if(!$order) {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка, заявка не найдена или уже завершена']);
            return $this->redirect(['site/'.$ref]);
        }
        $bot = new Bot;
        $status = $bot->cancelBid($bid_id);
        if($status) {
            Orders::cancelBid($bid_id);
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Заявка отменена успешно']);
        } else {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
        }
        return $this->redirect(['site/'.$ref]);
    }

    public function actionChangeStatus($mode, $status) {
        $status = Config::changeStatus($mode, $status ? 1 : 0);
        if($status) {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Статус изменен']);
        } else {
            \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
        }
        return $this->redirect(['site/index']);
    }

    public function actionTest($bid_id = 0) {
        $test_form = new TestForm;
        if($bid_id) {
            $test_form->loadByBid($bid_id);
        }
        if(\Yii::$app->request->post()) {
            $test_form->load(\Yii::$app->request->post());
            if($test_form->validate()) {
                if($bid_id) {
                    $result = $test_form->changeBid($bid_id);
                    if($result) {
                        \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Заявка изменена']);
                        $this->refresh();
                    } else {
                        \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
                    }
                } else {
                    $result = $test_form->makeBid();
                    if($result) {
                        \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Заявка создана']);
                        $this->refresh();
                    } else {
                        \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
                    }
                }
            } else {
                \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
            }
        }
        $orders = Orders::find()->where(['not', 'status', 'finished'])->all();
        return $this->render('test.twig', [
            'testing' => $test_form,
            'orders' => $orders,
            'bid_id' => $bid_id,
            'currency_list' => Config::$currency_list,
        ]);
    }

    public function actionConfig() {
        $config = Config::loadConfig(1);
        if(\Yii::$app->request->post()) {
            $config->load(\Yii::$app->request->post());
            if($config->validate() && $config->save()) {
                \Yii::$app->getSession()->setFlash('alert', ['type' => 'success', 'msg' => 'Настройки сохранены']);
            } else {
                \Yii::$app->getSession()->setFlash('alert', ['type' => 'danger', 'msg' => 'Ошибка']);
            }
        }
        return $this->render('config.twig', [
            'config' => $config,
            'currency_list' => Config::$currency_list,
            'frequency_list' => Config::$frequency_list
        ]);
    }

    public function actionStats($from = '', $to = '', $time_from = '', $time_to = '') {
        $query = Stats::find()->orderBy('timestamp DESC');
        if(!empty($from) || !empty($to)) {
            if(!empty($from)) {
                $query->andWhere(['>=', 'timestamp', strtotime($from)]);
            }
            if(!empty($to)) {
                $query->andWhere(['<=', 'timestamp', strtotime($to)]);
            }
        }
        if(!empty($time_from) || !empty($time_to)) {
            if(!empty($time_from)) {
                $time_from_parts = explode(':', $time_from);
                $query->andWhere(['>=', 'time_h', $time_from_parts[0]]);
                //$query->andWhere(['>=', 'time_m', $time_from_parts[1]]);
            }
            if(!empty($time_to)) {
                $time_to_parts = explode(':', $time_to);
                $query->andWhere(['<=', 'time_h', $time_to_parts[0]]);
                //$query->andWhere(['<=', 'time_m', $time_to_parts[1]]);
            }
        }
        $countQuery = clone $query;
        $cnt = $countQuery->count();
        $pages = new \yii\data\Pagination(['totalCount' => $cnt]);
        $stats = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->asArray()
        ->all();
        $stats = Stats::getTableView($stats);
        return $this->render('stats.twig', [
            'stats' => $stats,
            'stats_count' => $cnt,
            'pages' => $pages,
            'time_from' => $time_from,
            'time_to' => $time_to,
            'from' => $from,
            'to' => $to,
        ]);
    }

    public function actionExportStats($from = '', $to = '', $time_from = '', $time_to = '') {
        $query = Stats::find()->orderBy('timestamp DESC');
        if(!empty($from) || !empty($to)) {
            if(!empty($from)) {
                $query->andWhere(['>=', 'timestamp', strtotime($from)]);
            }
            if(!empty($to)) {
                $query->andWhere(['<=', 'timestamp', strtotime($to)]);
            }
        }
        if(!empty($time_from) || !empty($time_to)) {
            if(!empty($time_from)) {
                $time_from_parts = explode(':', $time_from);
                $query->andWhere(['>=', 'time_h', $time_from_parts[0]]);
                //$query->andWhere(['>=', 'time_m', $time_from_parts[1]]);
            }
            if(!empty($time_to)) {
                $time_to_parts = explode(':', $time_to);
                $query->andWhere(['<=', 'time_h', $time_to_parts[0]]);
                //$query->andWhere(['<=', 'time_m', $time_to_parts[1]]);
            }
        }
        \moonland\phpexcel\Excel::export([
            'models' => $query->all(),
            'columns' => Stats::getColumns(),
        ]);
    }

    public function actionLogs() {
        Events::cancel('error');
        $query = \app\models\Logs::find()->orderBy('time DESC');
        $countQuery = clone $query;
        $cnt = $countQuery->count();
        $pages = new \yii\data\Pagination(['totalCount' => $cnt]);
        $logs = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->all();
        $logs = array_map(function($l) { if(is_array($l->msg)) { $l->msg = print_r($l->msg, 1); } return $l; }, $logs);
        return $this->render('logs.twig', ['logs' => $logs, 'logs_count' => $cnt, 'pages' => $pages]);
    }

    public function actionLogin() {
        if (!\Yii::$app->user->isGuest) {
            return $this->redirect(['site/index']);
        }
        $model = new \app\models\LoginForm;
        if ($model->load(\Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login.twig', [
                'model' => $model,
            ]
        );
    }
}

public function actionLogout() {
    Yii::$app->user->logout();
    return $this->redirect(['site/index']);
}

}
