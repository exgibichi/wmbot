<?php
namespace app\commands;

use yii\console\Controller;
use \app\TradeLib\Parsers\YaParser;
use \app\TradeLib\Parsers\WMParser;
use \app\TradeLib\Checker;
use \app\TradeLib\Http;
use \app\TradeLib\Bot;
use \app\models\SessionsInfo;
use \app\models\Config;
use \app\models\Orders;
use \app\models\Events;
use \app\models\Stats;
use \app\models\Logs;
use \app\models\Rate;

class ManagerController extends Controller {

    public function actionIndex() {
        $checker = new Checker;
        $this->parsing($checker);
        $this->trading($checker);
        //$this->updateHandmade();
    }

    private function updateHandmade() { // todo add update time freq
        $bot = new Bot;
        $bid_ids = Orders::getActive('handmade');
        if(empty($bid_ids)) { return; }
        $this->checkBids($bot, $bid_ids);
    }

    private function trading($checker) {
        if(!$checker->check('trading')) {
            Logs::log('not traded reason: '.$checker->getError(), 'trade', Logs::SYSTEM_INFO);
            return;
        }
        SessionsInfo::updateLastTime('trading');
        $this->updateHandmade();
        $bot = new Bot;
        $bid_ids = Orders::getActive('botmade');
        if(empty($bid_ids)) {
            $this->addNewBid($bot);
            $bid_ids = Orders::getActive();
        }
        if(empty($bid_ids)) { return; }
        $this->checkBids($bot, $bid_ids);
    }

    private function checkBids(&$bot, $bid_ids) {
        $config = Config::loadConfig();
        $http = new Http;
        $wm_parser = new WMParser($http);
        foreach($bid_ids as $bid_id) {
            $order = Orders::find()->where(['bid_id' => $bid_id])->one();
            $info = $bot->getBidInfo($bid_id);
            if(!$info) { Logs::log('cant get bid info, bid_id: '.$bid_id, 'trade', Logs::SYSTEM_ERROR); continue; }
            $prepared_info = Orders::prepare($info);
            if($prepared_info['status'] == Orders::DELETED) {
                Logs::log('bid: '.$bid_id.' deleted', 'trade', Logs::SYSTEM_INFO);
                Orders::changeStatus($bid_id, 'deleted');
                Orders::changeStatus($bid_id, 'finished');
                Config::changeStatus('trading', 0);
                Events::addEvent('bid_deleted');
                continue;
            }
            $position = $wm_parser->findPostion($bid_id, $order->first_currency == 'wmz' ? 1 : 0);
            $ya_rate = Rate::getNewRate($config, Rate::RATE, 0);
            Orders::saveDiff($bid_id, [
                $prepared_info['first_sum'],
                $prepared_info['second_sum'],
                $prepared_info['rate'],
                $prepared_info['revert_rate'],
                $position,
                $ya_rate
            ], ['first_sum', 'second_sum', 'rate', 'revert_rate', 'position', 'ya_rate']);
            $result = Orders::compare($order, $prepared_info);
            if($result['status'] == Orders::SELLED) {
                Logs::log('bid selled, bid_id: '.$bid_id, 'trade', Logs::SYSTEM_SUCCESS);
                Orders::changeStatus($bid_id, 'finished');
                Config::changeStatus('trading', 0);
                Events::addEvent('bid_selled');
                continue;
            } else {
                if($prepared_info['status'] == Orders::PARTIAL_SELLED) {
                    $diff = Orders::checkDiff($bid_id, $info['first_sum'], 'first_sum');
                    if($diff) {
                        Orders::saveDiff($bid_id, $info['first_sum'], 'first_sum', 1);
                        Logs::log('partial sell: '.$diff, 'trade', Logs::SYSTEM_SUCCESS);
                        Events::addEvent('bid_partial_selled');
                    }
                }
                if($order->type == 'handmade') { continue; }

                $rate = Rate::getNewRate(
                $config,
                $order->first_currency == 'wmz' ? Rate::REVERT_RATE : Rate::RATE,
                $order->diff);

                $revert_rate = Rate::getNewRate(
                $config,
                $order->first_currency == 'wmz' ? Rate::RATE : Rate::REVERT_RATE,
                $order->diff);

                if(!$rate || !$revert_rate) {
                    Logs::log('cant get rate '.Rate::getError(), 'trade', Logs::SYSTEM_ERROR);
                    Config::changeStatus('trading', 0);
                    continue;
                }
                /*
                $diff = Orders::checkDiff($bid_id, $rate, 'rate');
                $revert_diff = Orders::checkDiff($bid_id, $revert_rate, 'revert_rate');
                */
                $sum_diff = 0;
                $sum_revert_diff = 0;
                if($rate > $revert_rate) {
                    $sum_diff = abs($order->second_sum - $order->first_sum/$rate);
                } else {
                    $sum_revert_diff = abs($order->second_sum - $order->first_sum*$revert_rate);
                }
                if($sum_diff !== 0) {
                    $new_rate = $rate;
                    $diff = $sum_diff;
                    $type = 0;
                } else {
                    $new_rate = $revert_rate;
                    $diff = $sum_revert_diff;
                    $type = 1;
                }
                Logs::log([
                    'bid_id' => $bid_id,
                    'rate' => $rate,
                    'revert_rate' => $revert_rate,
                    'new_rate' => $new_rate,
                    'sum_diff' => round($sum_diff, 4),
                    'sum_revert_diff' => round($sum_revert_diff, 4),
                    'type' => $type,
                ],
                'trade',
                Logs::SYSTEM_INSIDE);
                if(round(abs($diff), 2) > 0.01) {
                    $result = $bot->changeBid($bid_id, $new_rate, $type);
                    if(!$result) {
                        Logs::log('cant change bid error: '.$bot->getError(), 'trade', Logs::SYSTEM_ERROR);
                        continue;
                    }
                    $info = $bot->getBidInfo($bid_id);
                    if(!$info) { Logs::log('cant get bid info, bid_id: '.$bid_id, 'trade', Logs::SYSTEM_ERROR); continue; }
                    $prepared_info = Orders::prepare($info);
                    Orders::saveDiff($bid_id, [
                        $prepared_info['first_sum'],
                        $prepared_info['second_sum'],
                        $prepared_info['rate'],
                        $prepared_info['revert_rate'],
                        $position,
                        $ya_rate
                    ], ['first_sum', 'second_sum', 'rate', 'revert_rate', 'position', 'ya_rate']);
                    Events::addEvent('bid_changed');
                    Logs::log('change bid: '.$bid_id.' new rate: '.$rate, 'trade', Logs::SYSTEM_SUCCESS);
                } else {
                    Logs::log('bid rate not changed ', 'trade', Logs::SYSTEM_INFO);
                }
            }
        }
    }

    private function addNewBid(&$bot) {
        $config = Config::loadConfig();
        $balance = $bot->getBalance($config->getPurse($config->first_currency));
        if(!$balance) {
            Logs::log('cant get balance: '.$bot->getError(), 'trade', Logs::SYSTEM_ERROR);
            Config::changeStatus('trading', 0);
            return;
        }
        if($balance < $config->first_sum) {
            Logs::log('not enough money, curr balance: '.$balance.' need: '.$config->first_sum, 'trade', Logs::SYSTEM_ERROR);
            Config::changeStatus('trading', 0);
            return;
        }
        $rate = Rate::getNewRate($config, $config->first_currency == 'wmr' ? 1 : 0, $config->diff);
        if(!$rate) {
            Logs::log('cant get rate '.Rate::getError(), 'trade', Logs::SYSTEM_ERROR);
            Config::changeStatus('trading', 0);
            return;
        }
        $bid_id = $bot->makeBid([
            'first_sum' => $config->first_sum,
            'second_sum' => $config->first_sum*$rate,
            'first_purse' => $config->getPurse($config->first_currency),
            'second_purse' => $config->getPurse($config->second_currency)
        ]);
        if(!$bid_id) {
            Logs::log('cant make bid error: '.$bot->getError(), 'trade', Logs::SYSTEM_ERROR);
            return;
        }
        $bid = $bot->getBidInfo($bid_id);
        $wm_parser = new WMParser(new Http);
        $position = $wm_parser->findPostion($bid_id, $config->first_currency == 'wmz' ? 1 : 0);
        $ya_rate = Rate::getNewRate($config, Rate::RATE, 0);
        Orders::addBid(
        $bid_id,
        [
            'rate' => $this->format($bid['inoutrate']),
            'revert_rate' => $this->format($bid['outinrate']),
            'first_sum' => $this->format($bid['amountin']),
            'second_sum' => $this->format($bid['amountout']),
            'first_currency' => $config->first_currency,
            'second_currency' => $config->second_currency,
            'diff' => $config->diff,
            'position' => $position,
            'ya_rate' => $ya_rate
        ],
        'botmade');
        Events::addEvent('bid_add_new');
        SessionsInfo::updateLastTime('trading');
        Logs::log('add new bid link to history', 'trade', Logs::SYSTEM_SUCCESS);
    }

    private function format($data) {
        return str_ireplace(',', '.', $data);
    }

    private function parsing($checker) {
        if($checker->check('parsing')) {
            $http = new Http;
            //$http->setProxy('localhost', 666);
            $yahoo_parser = new YaParser($http);
            $wm_parser = new WMParser($http);
            $yahoo_result = $yahoo_parser->parse();
            if(!$checker->validate($yahoo_result, 'ya')) {
                Logs::log('yahoo parsing result invalid: '.$checker->getError(), 'parsing', Logs::SYSTEM_ERROR);
                return;
            }
            $wm_result = $wm_parser->parse();
            if(!$checker->validate($wm_result, 'wm')) {
                Logs::log('wm parsing result invalid: '.$checker->getError(), 'parsing', Logs::SYSTEM_ERROR);
                return;
            }
            Stats::add([$yahoo_result, $wm_result]);
            SessionsInfo::updateLastTime('parsing');
            Logs::log('parsed succseful', 'parsing', Logs::SYSTEM_SUCCESS);
        } else {
            Logs::log('not parsed reason: '.$checker->getError(), 'parsing', Logs::SYSTEM_INFO);
        }
    }
}
