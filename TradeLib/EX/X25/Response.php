<?php

namespace app\TradeLib\EX\X25;
use baibaratsky\WebMoney\Request\AbstractResponse;

/**
 * Class Response
 * delete bid
 * @link http://wm.exchanger.ru/asp/rules_xml.asp 5
 */
class Response extends AbstractResponse
{
    /** @var int reqn */
    protected $requestNumber;

    public function __construct($response)
    {
        parent::__construct($response);

        $responseObject = new \SimpleXMLElement($response);
        $this->returnCode = (int)$responseObject->retval;
        $this->returnDescription = (string)$responseObject->retdesc;
    }

}
