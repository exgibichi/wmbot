<?php

namespace app\TradeLib\EX\X25;
use baibaratsky\WebMoney\Api\X;
use baibaratsky\WebMoney\Exception\ApiException;
use baibaratsky\WebMoney\Request\RequestValidator;
use baibaratsky\WebMoney\Signer;

/**
 * Class Request
 * delete bid
 * @link http://wm.exchanger.ru/asp/rules_xml.asp 5
 */
class Request extends X\Request
{
    /** @var string getpurses/wmid */
    protected $requestedWmid;
    protected $capitallerWmid = '';

    public function __construct($authType = self::AUTH_CLASSIC)
    {
        switch ($authType) {
            case self::AUTH_CLASSIC:
                $this->url = 'https://wm.exchanger.ru/asp/XMLTransDel.asp';
                break;

            case self::AUTH_LIGHT:
                $this->url = 'https://wmeng.exchanger.ru/asp/XMLTransDel.asp';
                break;

            default:
                throw new ApiException('This interface doesn\'t support the authentication type given.');
        }

        parent::__construct($authType);
    }

    /**
     * @return array
     */
    protected function getValidationRules()
    {
        return array(
                RequestValidator::TYPE_REQUIRED => array('requestedWmid'),
                RequestValidator::TYPE_DEPEND_REQUIRED => array(
                        'signerWmid' => array('authType' => array(self::AUTH_CLASSIC)),
                ),
        );
    }

    /**
     * @return string
     */
    public function getData()
    {
        $xml = '<wm.exchanger.request>';
        $xml .= self::xmlElement('wmid', $this->requestedWmid);
        $xml .= self::xmlElement('signstr', $this->signature);
        $xml .= self::xmlElement('operid', $this->operId);
        $xml .= self::xmlElement('capitallerwmid', $this->capitallerWmid);
        $xml .= '</wm.exchanger.request>';

        return $xml;
    }

    /**
     * @return string
     */
    public function getResponseClassName()
    {
        return Response::className();
    }

    /**
     * @param Signer $requestSigner
     *
     */
    public function sign(Signer $requestSigner = null)
    {
        if ($this->authType === self::AUTH_CLASSIC) {
            $this->signature = $requestSigner->sign($this->requestedWmid . $this->requestNumber);
        }
    }

    /**
     * @return string
     */
    public function getRequestedWmid()
    {
        return $this->requestedWmid;
    }

    /**
     * @param string $requestedWmid
     */
    public function setRequestedWmid($requestedWmid)
    {
        $this->requestedWmid = (string)$requestedWmid;
    }

    /**
     * @return string
     */
    public function getOperId()
    {
        return $this->operId;
    }

    /**
     * @param string $operId
     */
    public function setOperId($operId)
    {
        $this->operId = (string)$operId;
    }

    /**
     * @return string
     */
    public function getCapitallerWmid()
    {
        return $this->capitallerWmid;
    }

    /**
     * @param string $capitallerWmid
     */
    public function setCapitallerWmid($capitallerWmid)
    {
        $this->capitallerWmid = (string)$capitallerWmid;
    }
}
