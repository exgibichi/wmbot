<?php

namespace app\TradeLib\EX\X24;
use baibaratsky\WebMoney\Request\AbstractResponse;

/**
* Class Response
* bids info
* @link http://wm.exchanger.ru/asp/rules_xml.asp 3
*/
class Response extends AbstractResponse
{
    /** @var int reqn */
    private $queries;

    public function __construct($response)
    {
        parent::__construct($response);

        $responseObject = new \SimpleXMLElement($response);
        $this->returnCode = (int)$responseObject->retval;
        $this->returnDescription = (string)$responseObject->retdesc;
        foreach ($responseObject->WMExchnagerQuerys->query as $query) {
            $query = (array)$query;
            $query = $query['@attributes'];
            $this->queries[$query['id']] = $query;
        }
    }

    public function getBids() {
        return $this->queries;
    }

    public function getBidById($bid_id) {
        if(isset($this->queries[$bid_id])) {
            return $this->queries[$bid_id];
        }
    }

}
