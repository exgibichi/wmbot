<?php

namespace app\TradeLib\EX\X23;

use baibaratsky\WebMoney\Request\AbstractResponse;

/**
 * Class Response
 * create bid
 * @link http://wm.exchanger.ru/asp/rules_xml.asp 8
 */
class Response extends AbstractResponse
{
    protected $bid_id;

    public function __construct($response)
    {
        parent::__construct($response);
        preg_match('/operid="(\d+)"/si', $response, $r);
        if(isset($r[1]) && !empty($r[1])) {
            $this->bid_id = $r[1];
        }
        $responseObject = new \SimpleXMLElement($response);
        $this->returnCode = (int)$responseObject->retval;
        $this->returnDescription = (string)$responseObject->retdesc;
    }

    public function getBidId() {
        return $this->bid_id;
    }

}
