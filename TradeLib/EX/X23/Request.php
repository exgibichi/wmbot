<?php

namespace app\TradeLib\EX\X23;

use baibaratsky\WebMoney\Api\X;
use baibaratsky\WebMoney\Exception\ApiException;
use baibaratsky\WebMoney\Request\RequestValidator;
use baibaratsky\WebMoney\Signer;

/**
 * Class Request
 * create bid
 * @link http://wm.exchanger.ru/asp/rules_xml.asp 8
 */
class Request extends X\Request {
    /** @var string getpurses/wmid */
    protected $requestedWmid;
    protected $capitallerWmid = '';

    public function __construct($authType = self::AUTH_CLASSIC)
    {
        switch ($authType) {
            case self::AUTH_CLASSIC:
                $this->url = 'https://wm.exchanger.ru/asp/XMLTrustPay.asp';
                break;

            case self::AUTH_LIGHT:
                $this->url = 'https://wmeng.exchanger.ru/asp/XMLTrustPay.asp';
                break;

            default:
                throw new ApiException('This interface doesn\'t support the authentication type given.');
        }

        parent::__construct($authType);
    }

    /**
     * @return array
     */
    protected function getValidationRules()
    {
        return array(
                RequestValidator::TYPE_REQUIRED => array('requestedWmid'),
                RequestValidator::TYPE_DEPEND_REQUIRED => array(
                        'signerWmid' => array('authType' => array(self::AUTH_CLASSIC)),
                ),
        );
    }

    /**
     * @return string
     */
    public function getData()
    {
        $xml = '<wm.exchanger.request>';
        $xml .= self::xmlElement('wmid', $this->requestedWmid);
        $xml .= self::xmlElement('signstr', $this->signature);
        $xml .= self::xmlElement('inpurse', $this->inPurse);
        $xml .= self::xmlElement('outpurse', $this->outPurse);
        $xml .= self::xmlElement('inamount', $this->inAmount);
        $xml .= self::xmlElement('outamount', $this->outAmount);
        $xml .= self::xmlElement('capitallerwmid', $this->capitallerWmid);
        $xml .= '</wm.exchanger.request>';
        return $xml;
    }

    /**
     * @return string
     */
    public function getResponseClassName()
    {
        return Response::className();
    }

    /**
     * @param Signer $requestSigner
     *
     */
    public function sign(Signer $requestSigner = null)
    {
        if ($this->authType === self::AUTH_CLASSIC) {
            $this->signature = $requestSigner->sign($this->requestedWmid . $this->requestNumber);
        }
    }

    /**
     * @return string
     */
    public function getRequestedWmid()
    {
        return $this->requestedWmid;
    }

    /**
     * @param string $requestedWmid
     */
    public function setRequestedWmid($requestedWmid)
    {
        $this->requestedWmid = (string)$requestedWmid;
    }

    /**
     * @return string
     */
    public function getInPurse()
    {
        return $this->inPurse;
    }

    /**
     * @param string $inPurse
     */
    public function setInPurse($inPurse)
    {
        $this->inPurse = (string)$inPurse;
    }
    /**
     * @return string
     */
    public function getOutPurse()
    {
        return $this->outPurse;
    }

    /**
     * @param string $outPurse
     */
    public function setOutPurse($outPurse)
    {
        $this->outPurse = (string)$outPurse;
    }
    /**
     * @return string
     */
    public function getInAmount()
    {
        return $this->inAmount;
    }

    /**
     * @param string $inAmount
     */
    public function setInAmount($inAmount)
    {
        $this->inAmount = (string)$inAmount;
    }
    /**
     * @return string
     */
    public function getOutAmount()
    {
        return $this->outAmount;
    }

    /**
     * @param string $outAmount
     */
    public function setOutAmount($outAmount)
    {
        $this->outAmount = (string)$outAmount;
    }
    /**
     * @return string
     */
    public function getCapitallerWmid()
    {
        return $this->capitallerWmid;
    }

    /**
     * @param string $capitallerWmid
     */
    public function setCapitallerWmid($capitallerWmid)
    {
        $this->capitallerWmid = (string)$capitallerWmid;
    }
}
