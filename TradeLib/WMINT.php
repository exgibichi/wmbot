<?php

namespace app\TradeLib;

use \baibaratsky\WebMoney\WebMoney;
use \baibaratsky\WebMoney\Request\Requester\CurlRequester;
use \baibaratsky\WebMoney\Api\X;
use app\TradeLib\EX;

class WMINT {
    private $wmid;
    private $wm;

    public function __construct() {
        $this->wm = new WebMoney(new CurlRequester);
    }

    public function setWMID($wmid) {
        $this->wmid = $wmid;
    }

    public function cancelBid($bid_id) {
        $request = new EX\X25\Request(EX\X25\Request::AUTH_LIGHT);
        $request->setRequestedWmid($this->wmid);
        $request->cert(\Yii::getAlias('@app/TradeLib/keys/mywm.pem'), \Yii::getAlias('@app/TradeLib/keys/mywm.key'));
        $request->setOperId($bid_id);
        if ($request->validate()) {
            $response = $this->wm->request($request);
            if ($response->getReturnCode() === 0) {
                return ['status' => 'success'];
            } else {
                return ['status' => 'error', 'msg' => 'Error: ' . $response->getReturnDescription()];
            }
        } else {
            return ['status' => 'error', 'msg' => 'cant validate'];
        }
    }

    public function changeBid($bid_id, $type, $rate) {
        $request = new EX\X26\Request(EX\X26\Request::AUTH_LIGHT);
        $request->setRequestedWmid($this->wmid);
        $request->cert(\Yii::getAlias('@app/TradeLib/keys/mywm.pem'), \Yii::getAlias('@app/TradeLib/keys/mywm.key'));
        $request->setOperId($bid_id);
        $request->setCursType($type);
        $request->setCursAmount($rate);
        if ($request->validate()) {
            $response = $this->wm->request($request);
            if ($response->getReturnCode() === 0) {
                return ['status' => 'success'];
            } else {
                return ['status' => 'error', 'msg' => 'Error: ' . $response->getReturnDescription()];
            }
        } else {
            return ['status' => 'error', 'msg' => 'cant validate'];
        }
    }

    public function getBidInfo($bid_id, $type = 3) {
        $request = new EX\X24\Request(EX\X24\Request::AUTH_LIGHT);
        $request->setRequestedWmid($this->wmid);
        $request->cert(\Yii::getAlias('@app/TradeLib/keys/mywm.pem'), \Yii::getAlias('@app/TradeLib/keys/mywm.key'));
        $request->setQueryId($bid_id);
        $request->setType($type);
        if ($request->validate()) {
            $response = $this->wm->request($request);
            if ($response->getReturnCode() === 0) {
                return ['status' => 'success', 'data' => $response->getBidById($bid_id)];
            } else {
                return ['status' => 'error', 'msg' => 'Error: ' . $response->getReturnDescription()];
            }
        } else {
            return ['status' => 'error', 'msg' => 'cant validate'];
        }
    }

    public function createBid($inPurse, $outPurse, $inAmount, $outAmount) {
        $request = new EX\X23\Request(EX\X23\Request::AUTH_LIGHT);
        $request->setRequestedWmid($this->wmid);
        $request->cert(\Yii::getAlias('@app/TradeLib/keys/mywm.pem'), \Yii::getAlias('@app/TradeLib/keys/mywm.key'));
        $request->setInPurse($inPurse);
        $request->setOutPurse($outPurse);
        $request->setInAmount($inAmount);
        $request->setOutAmount($outAmount);
        if ($request->validate()) {
            $response = $this->wm->request($request);
            if ($response->getReturnCode() === 0) {
                return ['status' => 'success', 'data' => $response->getBidId()];
            } else {
                return ['status' => 'error', 'msg' => 'Error: ' . $response->getReturnDescription()];
            }
        } else {
            return ['status' => 'error', 'msg' => 'cant validate'];
        }
    }

    public function getBalance($purse) {
        $request = new X\X9\Request(X\X9\Request::AUTH_LIGHT);
        $request->setRequestedWmid($this->wmid);
        $request->cert(\Yii::getAlias('@app/TradeLib/keys/mywm.pem'), \Yii::getAlias('@app/TradeLib/keys/mywm.key'));
        if ($request->validate()) {
            $response = $this->wm->request($request);
            if ($response->getReturnCode() === 0) {
                if($response->getPurseByName($purse)) {
                    return ['status' => 'success', 'data' => $response->getPurseByName($purse)->getAmount()];
                } else {
                    return ['status' => 'error', 'msg' => 'Error: ' . 'нет такого кошелька'];
                }
            } else {
                return ['status' => 'error', 'msg' => 'Error: ' . $response->getReturnDescription()];
            }
        } else {
            return ['status' => 'error', 'msg' => 'cant validate'];
        }
    }

}
