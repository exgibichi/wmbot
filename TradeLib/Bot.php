<?php
namespace app\TradeLib;

use \app\models\Config;
use \app\models\Logs;

class Bot {
    private $error;

    public function __construct() {
        $config = Config::loadConfig();
        $this->wm = new WMINT;
        $this->wm->setWMID($config->wmid);
    }

    public function getBalance($purse) {
        if(empty($purse)) {
            $this->error = 'empty purse';
            return;
        }
        $result = $this->wm->getBalance($purse);
        if($result['status'] == 'error') {
            $this->error = $result['msg'];
            return false;
        } else {
            $balance = $result['data'];
        }
        $log = ['function' => __FUNCTION__, 'input' => ['purse' => $purse], 'output' => ['result' => $result]];
        Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
        return $balance;
    }

    public function getBidInfo($bid_id) {
        $result = $this->wm->getBidInfo($bid_id);
        $log = ['function' => __FUNCTION__, 'input' => ['bid_id' => $bid_id], 'output' => ['result' => $result]];
        if($result['status'] == 'error') {
            $this->error = $result['msg'];
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return false;
        } else {
            $result = $result['data'];
        }
        Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
        return $result;
    }

    public function changeBid($bid_id, $rate, $type = 0) {
        $rate = round($rate, 4);
        $result = $this->wm->changeBid($bid_id, $type, $rate);
        $log = ['function' => __FUNCTION__, 'input' => ['bid_id' => $bid_id, 'new_rate' => $rate, 'type' => $type], 'output' => ['result' => $result]];
        if($result['status'] == 'error') {
            $this->error = $result['msg'];
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return false;
        } else {
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return true;
        }
    }

    public function makeBid($data) {
        $data['first_sum'] = round($data['first_sum'], 2);
        $data['second_sum'] = round($data['second_sum'], 2);
        $result = $this->wm->createBid($data['first_purse'], $data['second_purse'], $data['first_sum'], $data['second_sum']);
        $log = ['function' => __FUNCTION__, 'input' => ['data' => $data], 'output' => ['result' => $result]];
        if($result['status'] == 'error') {
            $this->error = $result['msg'];
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return false;
        } else {
            $bid_id = $result['data'];
        }
        Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
        return $bid_id;
    }

    public function cancelBid($bid_id) {
        $result = $this->wm->cancelBid($bid_id);
        $log = ['function' => __FUNCTION__, 'input' => ['bid_id' => $bid_id], 'output' => ['result' => $result]];
        if($result['status'] == 'error') {
            $this->error = $result['msg'];
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return false;
        } else {
            Logs::log($log, 'bot', Logs::SYSTEM_INSIDE);
            return $result;
        }
    }

    public function getError() {
        return $this->error;
    }

}
