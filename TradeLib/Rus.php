<?php

namespace app\TradeLib;

class Rus {
    private $text = [
        //status
        'new' => 'Новая',
        'changed' => 'Изменена',
        'checked' => 'Проверена',
        'canceled' => 'Отменена',
        'deleted' => 'Удалена',
        'finished' => 'Завершена',
        //bid type
        'botmade' => 'Авто',
        'handmade' => 'Ручная',
        // orders
        'rate' => 'курс',
        'revert_rate' => 'обратный курс',
        'first_sum' => 'есть сумма',
        'second_sum' => 'нужна сумма',
        'position' => 'позиция',
        'diff' => 'дифф'
    ];

    public static function t($text) {
        $r = new Rus;
        return isset($r->text[$text]) ? $r->text[$text] : $text;
    }
}
