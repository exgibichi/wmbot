<?php
namespace app\TradeLib;

use \app\models\Orders;
use \app\models\Config;
use \app\models\SessionsInfo;

class Checker {
    private $error = 'none';
    const RUNNING = 1;
    const STOPPED = 0;
    public function check($type) {
        if($type == 'has_bid') {
            return $this->checkBids();
        }
        $config = Config::loadConfig();
        if(!$config) { $this->error = 'can`t load config'; return; }
        $config = \yii\helpers\ArrayHelper::toArray($config);
        if(isset($config[$type]) && $config[$type] == self::STOPPED) { $this->error = $type.' stopped'; return; }
        if(!$this->checkTimeout($config['frequency'], $type)) { $this->error = 'time not come yet'; return; }
        return true;
    }

    private function checkBids() {
        return Orders::find()
        ->where(['not', 'status', 'finished'])
        ->count();
    }

    public function validate($num, $type) {
        if($type == 'ya') {
            if($num === 0 || empty($num)) { $this->error = 'empty result'; return; }
            if(!is_numeric($num)) { $this->error = 'not numeric: '.$num; return; }
        } elseif($type == 'wm') {
            if(empty($num['wmz'])) { $this->error = 'empty result'; return; }
            if(empty($num['wmr'])) { $this->error = 'empty result'; return; }
        }
        return true;
    }

    public function checkTimeout($timeout, $type) {
        $timeout = Config::$frequency_list[$timeout]*60;
        $si = SessionsInfo::find()->asArray()->one();
        if(!$si || !isset($si[$type]) || (time() - $si[$type] > $timeout)) {
            return true;
        }
    }

    public function getError() {
        return $this->error;
    }
}

?>
