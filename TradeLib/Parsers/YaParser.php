<?php
namespace app\TradeLib\Parsers;

class YaParser {
    private $h;
    private $link = 'https://finance.yahoo.com/quote/USDRUB=X?ltr=1';
    private static $ext_link = 'https://finance.yahoo.com/quote/USDRUB=X?ltr=1';

    public function __construct($http) {
        $this->h = $http;
    }

    public static function getLinks() {
        return self::$ext_link;
    }

    public function parse() {
        $html = $this->h->get($this->link);
        preg_match('/<!-- react-text: 36 -->(.*?)<!-- \/react-text -->/si', $html, $r);
        return isset($r[1]) ? trim($r[1]) : false;
    }
}

?>
