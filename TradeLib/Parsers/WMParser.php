<?php
namespace app\TradeLib\Parsers;

class WMParser {
    private $h;
    private $links = [
        'https://wmeng.exchanger.ru/asp/XMLWMList.asp?exchtype=2',
        'https://wmeng.exchanger.ru/asp/XMLWMList.asp?exchtype=1'
    ];
    private static $ext_links = [
        'http://wm.exchanger.ru/asp/wmlist.asp?exchtype=1',
        'http://wm.exchanger.ru/asp/wmlist.asp?exchtype=2',
    ];

    public function __construct($http) {
        $this->h = $http;
    }

    public static function getLinks($data) {
        return self::$ext_links[$data];
    }

    public function findPostion($bid_id, $type) {
        $position = '-';
        $xml = $this->h->get($this->links[$type]);
        if(!$xml) { return $postion; }
        $xml = simplexml_load_string($xml);
        $i = 1;
        foreach($xml->WMExchnagerQuerys->query as $q) {
            $q = (array)$q;
            if($bid_id == $q['@attributes']['id']) {
                $position = $i;
                break;
            }
            $i++;
        }
        return $position;
    }

    public function parse() {
        $pack = [];
        $xml = $this->h->get($this->links[0]);
        if(!$xml) { return; }
        $xml = simplexml_load_string($xml);
        $xml = (array)$xml->WMExchnagerQuerys->query[0];
        $pack['wmr']['course'] = str_ireplace(',', '.', $xml['@attributes']['inoutrate']);
        $xml = $this->h->get($this->links[1]);
        if(!$xml) { return; }
        $xml = simplexml_load_string($xml);
        $xml = (array)$xml->WMExchnagerQuerys->query[0];
        $pack['wmz']['course'] = str_ireplace(',', '.', $xml['@attributes']['outinrate']);
        $session = \app\models\SessionsInfo::find()->one();
        if($session) {
            $session->wmr = $pack['wmr']['course'];
            $session->wmz = $pack['wmz']['course'];
            $session->save();
        }
        return $pack;
    }
}

?>
