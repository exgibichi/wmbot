<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'layout' => 'main.twig',
    'components' => [
        'session' => [
            'class' => 'yii\mongodb\Session',
            'sessionCollection' => 'sessions',
        ],
        'mongodb' => [
            'class' => '\yii\mongodb\Connection',
            'dsn' => 'mongodb://localhost:27017/wmbot',
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'hZw_rOBqIZvwf602kBbnKtrIhuuzARaF',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'view' => [
            'renderers' => [
                'twig' => [
                    'class' => 'yii\twig\ViewRenderer',
                    // set cachePath to false in order to disable template caching
                    'cachePath' => '@runtime/Twig/cache',
                    // Array of twig options:
                    'options' => [
                        'auto_reload' => true,
                    ],
                    'functions' => [
                        'rus' => '\app\TradeLib\Rus::t',
                        new \Twig_SimpleFunction('links', function ($type, $data) {
                            if($type == 'wm') {
                                return \app\TradeLib\Parsers\WMParser::getLinks($data);
                            } elseif($type == 'ya') {
                                return \app\TradeLib\Parsers\YaParser::getLinks($data);
                            } elseif($type == 'bid') {
                                return \app\models\Orders::getLinks($data);
                            }
                        }),
                    ],
                    'globals' => [
                        'html' => '\yii\helpers\Html',
                    ]
                ],
            ],
        ],
        /*
        'urlManager' => [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'rules' => [
    ],
],
*/
],
'params' => $params,
];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug']['class'] = 'yii\debug\Module';
    $config['modules']['debug']['panels']['mongodb'] = ['class' => 'yii\\mongodb\\debug\\MongoDbPanel'];
    $config['modules']['debug']['allowedIPs'] = ['127.0.0.1', '::1', '83.172.43.57', '37.21.165.232'];
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
