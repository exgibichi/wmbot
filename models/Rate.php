<?php

namespace app\models;

use \app\models\Stats;

class Rate {
    private static $error;
    const RATE = 0;
    const REVERT_RATE = 1;
    const BOTH = 2;

    public static function getNewRate(&$config, $revert, $diff) {
        $stats = Stats::find()->limit(2)->orderBy('timestamp DESC')->select(['data'])->asArray()->all();
        $diff_abs = $config->diff_abs;
        if(!self::checkDiff($stats, $diff_abs)) {
            return false;
        }
        $last_course = $stats[0]['data'][0];
        $current_course = stristr($diff, '-') ? $last_course-str_ireplace('-', '', $diff) : $last_course+str_ireplace('+', '', $diff);
        $course = round($current_course, 4);
        $revert_course = round(1/$course, 4);
        if($revert == self::RATE) {
            $rate = $course;
        } elseif($revert == self::REVERT_RATE) {
            $rate = $revert_course;
        } else {
            $rate = ['rate' => $course, 'revert_rate' => $revert_course];
        }
        $log = ['function' => __FUNCTION__, 'input' => ['revert' => $revert, 'diff' => $diff, 'last_course' => $last_course], 'output' => ['current_course' => $current_course, 'rate' => $course, 'revert_rate' => $revert_course]];
        Logs::log($log, 'rate', Logs::SYSTEM_INSIDE);
        return $rate;
    }

    public static function checkDiff($stats, $diff_abs) {
        $last_course = $stats[0]['data'][0];
        $prelast_course = $stats[0]['data'][0];
        $diff = abs($last_course-$prelast_course);
        $min = min([$last_course, $prelast_course]);
        $diff_in_percent = ($diff*100)/$min;
        if($diff_in_percent > $diff_abs) {
            self::$error = 'values: '.$last_course.','.$prelast_course.' count diff: '.$diff_in_percent.' > '.$diff_abs;
            return false;
        }
        return true;
    }

    public static function getError() {
        return self::$error;
    }
}
