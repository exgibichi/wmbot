<?php

namespace app\models;

class Logs extends \yii\mongodb\ActiveRecord {
    const SYSTEM_INFO = 1;
    const SYSTEM_ERROR = 2;
    const SYSTEM_SUCCESS = 3;
    const SYSTEM_INSIDE = 4;
    public function rules() {
        return [
            [['_id','time', 'msg', 'tag', 'type'], 'safe'],
        ];
    }

    public static function log($msg, $tag, $type) {
        $log = new self;
        $log->time = microtime(true);
        $log->msg = $msg;
        $log->tag = $tag;
        $log->type = $type;
        if($type == self::SYSTEM_ERROR) {
            \app\models\Events::addEvent('system_error');
        }
        $log->save();
    }

    public static function collectionName() {
        return ['wmbot', 'logs'];
    }

    public function attributes() {
        return ['_id', 'time', 'msg', 'tag', 'type'];
    }

}
