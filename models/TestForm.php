<?php
namespace app\models;
use \app\TradeLib\Bot;

class TestForm extends \yii\base\Model {

    public $first_currency;
    public $first_sum;
    public $second_currency;
    public $second_sum;
    public $rate;
    public $revert_rate;

    public function loadByBid($bid_id) {
        $order = Orders::find()
        ->where(['bid_id' => $bid_id])
        ->andWhere(['not', 'status', 'finished'])
        ->one();
        if(!$order) {
            $this->addError('first_currency', 'Нет такой заявки или она уже исполнена');
            return;
        }
        $this->first_currency = $order->first_currency;
        $this->first_sum = $order->first_sum;
        $this->second_currency = $order->second_currency;
        $this->second_sum = $order->second_sum;
    }

    public function makeBid() {
        $bot = new Bot;
        $config = Config::loadConfig();
        if(!$this->checkConfig($config)) { return false; }
        $balance = $bot->getBalance($config->getPurse($this->first_currency));
        if($balance && $balance < $this->first_sum) {
            $this->addError('first_sum', 'недостаточно средств, ваш баланс: '.$balance);
            return false;
        }
        if(!$balance) {
            $this->addError('first_sum', $bot->getError());
            return false;
        }
        $bid_id = $bot->makeBid([
            'first_sum' => $this->first_sum,
            'second_sum' => $this->second_sum,
            'first_purse' => $config->getPurse($this->first_currency),
            'second_purse' => $config->getPurse($this->second_currency)
        ]);
        if(!$bid_id) {
            Logs::log('cant make bid error: '.$bot->getError(), 'hand', Logs::SYSTEM_ERROR);
            $this->addError('first_sum', 'Бот не смог сделать заявку: '.$bot->getError());
            return;
        }
        $bid = $bot->getBidInfo($bid_id);
        $ya_rate = Rate::getNewRate($config, Rate::RATE, 0);
        Orders::addBid(
            $bid_id,
            [
                'rate' => $this->format($bid['inoutrate']),
                'revert_rate' => $this->format($bid['outinrate']),
                'first_sum' => $this->format($bid['amountin']),
                'second_sum' => $this->format($bid['amountout']),
                'first_currency' => $this->first_currency,
                'second_currency' => $this->second_currency,
                'diff' => 0,
                'position' => 0,
                'ya_rate' => $ya_rate
            ],
            'handmade');
            Logs::log('add new handmade bid link to history', 'hand', Logs::SYSTEM_SUCCESS);
            return true;
        }

        public function changeBid($bid_id) {
            $bot = new Bot;
            $config = Config::loadConfig();
            if(!$this->checkConfig($config)) { return false; }
            $balance = $bot->getBalance($config->getPurse($this->first_currency));
            $order = Orders::find()
            ->where(['bid_id' => $bid_id])
            ->andWhere(['not', 'status', 'finished'])
            ->select(['first_sum', 'type'])
            ->one();
            if(!$order) {
                $this->addError('first_currency', 'Нет такой заявки или она уже исполнена');
                return;
            }
            $result = $bot->changeBid($bid_id, round($this->first_sum/$this->second_sum, 2));
            if(!$result) {
                Logs::log('cant make bid error: '.$bot->getError(), 'hand', Logs::SYSTEM_ERROR);
                $this->addError('first_sum', 'Бот не смог изменить заявку: '.$bot->getError());
                return;
            }
            $bid = $bot->getBidInfo($bid_id);
            Orders::saveDiff(
                $bid_id,
                [
                    $this->format($bid['inoutrate']),
                    $this->format($bid['outinrate']),
                    $this->format($bid['amountout']),
                ], ['rate', 'revert_rate', 'second_sum']);
                Logs::log('change handmade bid success', 'hand', Logs::SYSTEM_SUCCESS);
                return true;
            }

            private function format($data) {
                return str_ireplace(',', '.', $data);
            }

            private function checkConfig(&$config) {
                $error = 0;
                if(empty($config->wmid)) {
                    $this->addError('first_currency', 'заполните поле WMID в конфигурации');
                    $error = 1;
                }
                if(empty($config->wmz)) {
                    $this->addError('first_currency', 'заполните поле WMZ в конфигурации');
                    $error = 1;
                }
                if(empty($config->wmr)) {
                    $this->addError('first_currency', 'заполните поле WMR в конфигурации');
                    $error = 1;
                }
                if(!$error) {
                    return true;
                }
            }

            public function init() {
                $this->first_currency = 0;
                $this->second_currency = 0;
            }

            public function rules() {
                return [
                    [
                        [
                            'first_currency',
                            'first_sum',
                            'second_currency',
                            'second_sum',
                        ], 'required'
                    ],
                    [
                        [
                            'first_currency',
                            'first_sum',
                            'second_currency',
                            'second_sum'
                        ], 'safe'
                    ],
                ];
            }

            public function attributes() {
                return [
                    'first_currency',
                    'first_sum',
                    'second_currency',
                    'rate',
                    'revert_rate',
                    'second_sum'
                ];
            }

            public function attributeLabels() {
                return [
                    'first_currency' => 'Есть:',
                    'first_sum' => 'Есть сумма:',
                    'second_currency' => 'Нужно:',
                    'second_sum' => 'Нужна сумма:',
                    'rate' => 'Курс:',
                    'revert_rate' => 'Обратный курс:',
                ];
            }

        }
