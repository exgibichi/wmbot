<?php
namespace app\models;

class SessionsInfo extends \yii\mongodb\ActiveRecord {
    public function rules() {
        return [
            [['_id', 'parsing', 'trading', 'wmz', 'wmr'], 'safe'],
        ];
    }

    public static function updateLastTime($mode) {
        $si = self::find()->one();
        if(!$si) {
            $si = new SessionsInfo;
        }
        $si->$mode = time();
        $si->save();
    }

    public static function collectionName() {
        return ['wmbot', 'sessions_info'];
    }

    public function attributes() {
        return ['_id', 'parsing', 'trading', 'wmz', 'wmr'];
    }

}
