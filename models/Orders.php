<?php

namespace app\models;

class Orders extends \yii\mongodb\ActiveRecord {
    const SELLED = 1;
    const PARTIAL_SELLED = 2;
    const WAIT = 3;
    const DELETED = 4;
    private static $ext_link = 'https://wm.exchanger.ru/asp/wmtransdet.asp?operID=';

    public function rules() {
        return [
            [[
                '_id',
                'time',
                'bid_id',
                'rate',
                'revert_rate',
                'ya_rate',
                'diff',
                'position',
                'first_currency',
                'second_currency',
                'first_sum',
                'second_sum',
                'status',
                'history',
                'type'
            ],
            'safe'],
        ];
    }

    public static function getLinks($bid_id) {
        return self::$ext_link.$bid_id;
    }

    public static function prepare($raw_info) {
        /*
        0 - заявка еще не оплачена
        1 - оплачена, идет обмен
        2 - погашена полностью
        3 - объединена с другой новой
        4 - удалена, средства не возвращены
        5 - удалена, средства возвращены
        */
        $states = [
            '0' => self::WAIT,
            '1' => self::WAIT,
            '2' => self::SELLED,
            '3' => self::WAIT,
            '4' => self::DELETED,
            '5' => self::DELETED
        ];
        $prepared_info = [];
        $prepared_info['first_sum'] = self::format($raw_info['amountin']);
        $prepared_info['second_sum'] = self::format($raw_info['amountout']);
        $prepared_info['rate'] = self::format($raw_info['inoutrate']);
        $prepared_info['revert_rate'] = self::format($raw_info['outinrate']);
        $prepared_info['status'] = $states[$raw_info['state']];
        return $prepared_info;
    }

    private static function format($str) { // todo unify for all
        return str_ireplace(',', '.', $str);
    }

    public static function compare($order, $info) {
        if($info['status'] != self::WAIT) { return $info; }
        if($info['first_sum'] != $order->first_sum) {
            $info['status'] = self::PARTIAL_SELLED;
        }
        return $info;
    }

    public static function compileHistory($history) {
        $cnt = count($history);
        $compile = function($h) use($cnt) {
            static $i;
            static $base;
            if(!isset($i)) {
                $i = 1;
                $base = $h;
                $h['msg'] = 'Создание заявки';
            } else {
                foreach($base as $k => $v) {
                    if($k == 'time') { continue; }
                    if($k == 'ya_rate') { continue; }
                    if($k == 'status') {
                        if($h[$k] == 'canceled') {
                            $h['msg'] = 'Отмена заявки';
                        }
                        continue;
                    }
                    if(!isset($h[$k])) {
                        $h[$k] = $v;
                    } else {
                        if($k == 'type') {
                            if($h[$k] != $v) {
                                if($h[$k] == 'handmade') {
                                    $h['msg'] = 'Изменение режима на ручной';
                                } else {
                                    $h['msg'] = 'Изменение режима на автоматический';
                                }
                            }
                        } elseif($k != 'rate' || $k != 'revert_rate') {
                            if($h[$k] != $v) {
                                if(empty($h['msg'])) {
                                    $h['msg'] = 'Изменение '.\app\TradeLib\Rus::t($k).' '.round(abs($v-$h[$k]), 4);
                                } else {
                                    $h['msg'] .= '<br/>Изменение '.\app\TradeLib\Rus::t($k).' '.round(abs($v-$h[$k]), 4);
                                }
                            }
                        }
                        $base[$k] = $h[$k];
                    }
                }
                $i++;
                if($i == $cnt && $h['status'] == 'finished') {
                    $h['msg'] = 'Закрытие заявки';
                }
            }
            return $h;
        };
        return array_reverse(array_map($compile, $history));
    }

    public static function checkDiff($bid_id, $value, $type) {
        $order = self::find()->where(['bid_id' => $bid_id])->select([$type])->one();
        return $order->$type-$value;
    }

    public static function saveDiff($bid_id, $value, $type, $changed = 0) {
        $order = self::find()->where(['bid_id' => $bid_id])->one();
        if(!$order) { return; }
        $order->time = microtime(true);
        $order->status = $changed ? 'changed' : 'checked';
        $history = $order->history;
        if(is_array($value)) {
            $buff_history = ['time' => $order->time, 'status' => $order->status];
            foreach($type as $i => $key) {
                $order->$key = $value[$i];
                $buff_history[$key] = $order->$key;
            }
            $history[] = $buff_history;
        } else {
            $order->$type = $value;
            $history[] = ['time' => $order->time, $type => $order->$type, 'status' => $order->status];
        }
        $order->history = $history;
        return $order->save();
    }

    public static function getActive($type = 'all') {
        $orders_type = $type == 'all' ? ['botmade', 'handmade'] : ($type == 'botmade' ? 'botmade' : 'handmade');
        $orders = self::find()
        ->where(['not', 'status', 'finished'])
        ->andWhere(['type' => $orders_type])
        ->select(['bid_id'])
        ->asArray()
        ->all();
        $bid_ids = \yii\helpers\ArrayHelper::getColumn($orders, 'bid_id');
        return $bid_ids;
    }

    public static function cancelBid($bid_id) {
        self::changeStatus($bid_id, 'canceled');
        self::changeStatus($bid_id, 'finished');
    }

    public static function changeStatus($bid_id, $status) {
        $order = self::find()->where(['bid_id' => $bid_id])->one();
        $order->time = microtime(true);
        $order->status = $status;
        $history = $order->history;
        $history[] = ['time' => $order->time, 'status' => $order->status];
        $order->history = $history;
        $order->save();
    }

    public static function addBid($bid_id, $data, $type = 'botmade') {
        $order = new self;
        $order->time = microtime(true);
        $order->bid_id = $bid_id;
        $order->rate = floatval($data['rate']);
        $order->revert_rate = floatval($data['revert_rate']);
        $order->ya_rate = floatval($data['ya_rate']);
        $order->diff = $data['diff'];
        $order->position = $data['position'];
        $order->first_currency = $data['first_currency'];
        $order->first_sum = floatval($data['first_sum']);
        $order->second_currency = $data['second_currency'];
        $order->second_sum = floatval($data['second_sum']);
        $order->status = 'new';
        $order->type = $type;
        $order->history = [
            [
                'time' => $order->time,
                'bid_id' => $order->bid_id,
                'rate' => $order->rate,
                'revert_rate' => $order->revert_rate,
                'ya_rate' => $order->ya_rate,
                'diff' => $order->diff,
                'position' => $order->position,
                'first_currency' => $order->first_currency,
                'second_currency' => $order->second_currency,
                'first_sum' => $order->first_sum,
                'second_sum' => $order->second_sum,
                'status' => $order->status,
                'type' => $order->type
            ]
        ];
        $order->save();
    }

    public static function collectionName() {
        return ['wmbot', 'orders'];
    }

    public function attributes() {
        return [
            '_id',
            'time',
            'bid_id',
            'rate',
            'revert_rate',
            'ya_rate',
            'diff',
            'position',
            'first_currency',
            'second_currency',
            'first_sum',
            'second_sum',
            'status',
            'history',
            'type'
        ];
    }
}
