<?php
namespace app\models;

class Config extends \yii\mongodb\ActiveRecord {
    public static $currency_list = ['wmz' => 'WMZ', 'wmr' => 'WMR'];
    public static $frequency_list = [2,3,5,7,10,15,30];

    public function rules() {
        return [
            [[
                'diff',
                'wmid',
                'wmz',
                'wmr',
                'first_currency',
                'first_sum',
                'second_currency',
                'frequency',
                'diff',
                'diff_abs',
                'sound',
            ], 'required'],
            [['_id',
            'diff',
            'wmid',
            'wmz',
            'wmr',
            'first_currency',
            'first_sum',
            'second_currency',
            'frequency',
            'diff',
            'diff_abs',
            'sound',], 'safe'],
        ];
    }

    public function getPurse($purse_type) {
        $type = strtolower(self::$currency_list[$purse_type]);
        if(!empty($this->$type)) {
            return $this->$type;
        }
    }

    public static function changeStatus($mode, $status) {
        $config = self::loadConfig();
        if($config) {
            $config->$mode = $status;
            return $config->save();
        }
    }

    public static function loadConfig($force_create = 0) {
        $config = self::find()->one();
        if($config) {
            return $config;
        } elseif($force_create) {
            return new self;
        } else {
            return false;
        }
    }

    public static function collectionName() {
        return ['wmbot', 'config'];
    }

    public function attributes() {
        return [
            '_id',
            'diff',
            'wmid',
            'wmz',
            'wmr',
            'first_currency',
            'first_sum',
            'second_currency',
            'frequency',
            'diff',
            'diff_abs',
            'sound',
            'parsing',
            'trading'
        ];
    }

    public function attributeLabels() {
        return [
            'wmid' => 'WMID:',
            'wmz' => 'WMZ:',
            'wmr' => 'WMR:',
            'first_currency' => 'Есть:',
            'first_sum' => 'Сумма для заявки:',
            'second_currency' => 'Нужно:',
            'frequency' => 'Частота изменения курса заявки мин.:',
            'diff' => 'Дифференциал:',
            'diff_abs' => 'Отклонение для остановки %:',
            'sound' => 'Звуковое оповещение',
            'parsing',
            'trading'
        ];
    }

    public function beforeSave($insert) {
        $error = 0;
        if(!is_numeric($this->wmid)) {
            $this->addError('wmid', 'Должен состоять только из цифр');
            $error = 1;
        }
        if(strlen($this->wmid) !== 12) {
            $this->addError('wmid', 'Длина должна быть 12 знаков');
            $error = 1;
        }
        if(strlen($this->wmz) !== 13) {
            $this->addError('wmz', 'Длина должна быть 13 знаков');
            $error = 1;
        }
        if(strlen($this->wmr) !== 13) {
            $this->addError('wmr', 'Длина должна быть 13 знаков');
            $error = 1;
        }
        if(!stristr($this->wmz, 'z')) {
            $this->addError('wmz', 'Формат данных неверный');
            $error = 1;
        }
        if(!stristr($this->wmr, 'r')) {
            $this->addError('wmr', 'Формат данных неверный');
            $error = 1;
        }
        if(!is_numeric($this->first_sum)) {
            $this->addError('first_sum', 'Должна состоять только из цифр');
            $error = 1;
        }
        $this->diff = str_ireplace(',', '.', $this->diff);
        $this->diff_abs = str_ireplace(',', '.', $this->diff_abs);
        if(!is_numeric($this->diff)) {
            $this->addError('diff', 'Должен состоять только из цифр');
            $error = 1;
        }
        if(!is_numeric($this->diff_abs)) {
            $this->addError('diff_abs', 'Должно состоять только из цифр');
            $error = 1;
        }
        if($error) { return false; }
        return parent::beforeSave($insert);
    }

}
