<?php

namespace app\models;

class Stats extends \yii\mongodb\ActiveRecord {
    public function rules() {
        return [
            [['_id', 'timestamp', 'time_h', 'time_m', 'data'], 'safe'],
        ];
    }

    public static function add($pack) {
        $stat = new self;
        $stat->timestamp = microtime(true);
        $stat->time_h = date('H', time());
        $stat->time_m = date('i', time());
        $stat->data = $pack;
        $stat->save();
    }

    public static function getColumns() {
        $columns = [
            'id' => [
                'attribute' => '_id',
                'header' => 'Номер',
                'format' => 'raw',
                'value' => function($data) {
                    static $i;
                    if(empty($i)) {
                        $i = 1;
                    } else {
                        $i++;
                    }
                    return $i;
                }
            ],
            'date' => [
                'attribute' => 'time',
                'header' => 'Дата',
                'format' => 'raw',
                'value' => function($data) {
                    return date('d-m-Y', $data->timestamp);
                }
            ],
            'time' => [
                'attribute' => 'time',
                'header' => 'Время',
                'format' => 'raw',
                'value' => function($data) {
                    return date('H:i:s', $data->timestamp);
                }
            ],
            'wmz' => [
                'attribute' => 'time',
                'header' => 'Курс лучшей заявки WMZ',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->data[1]['wmz']['course'];
                }
            ],
            'wmr' => [
                'attribute' => 'time',
                'header' => 'Курс лучшей заявки WMR',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->data[1]['wmr']['course'];
                }
            ],
            'ya' => [
                'attribute' => 'time',
                'header' => 'Курс Yahoo finance',
                'format' => 'raw',
                'value' => function($data) {
                    return $data->data[0];
                }
            ],
        ];
        return $columns;
    }

    public static function getTableView($stats) {
        $sums = ['wmz' => 0, 'wmr' => 0, 'ya' => 0];
        $stats['entries'] = array_map(function($s) use(&$sums) { // todo hide
            static $last;
            $pack = [];
            $pack['time'] = round($s['timestamp']);
            $sums['wmz'] += $wmz = $s['data'][1]['wmz']['course'];
            $sums['wmr'] += $wmr = $s['data'][1]['wmr']['course'];
            $sums['ya'] += $ya = $s['data'][0];
            $pack['wmz'] = $wmz.(isset($last['wmz']) ? ' ('.round($wmz-$last['wmz'], 4).')' : '');
            $pack['wmr'] = $wmr.(isset($last['wmr']) ? ' ('.round($wmr-$last['wmr'], 4).')' : '');
            $pack['ya'] = $ya.(isset($last['ya']) ? ' ('.round($ya-$last['ya'], 4).')' : '');
            $last = $pack;
            return $pack;
        }, $stats);
        $cnt = count($stats['entries']);
        $stats['avg'] = [];
        if($cnt) {
            $stats['avg'] = array_map(function($s) use($cnt) { return round($s/$cnt, 4); }, $sums);
        }
        return $stats;
    }

    public static function collectionName() {
        return ['wmbot', 'new_stats'];
    }

    public function attributes() {
        return ['_id', 'timestamp', 'time_h', 'time_m', 'data'];
    }

    public function attributeLabels() {
        return [
            '_id', 'timestamp', 'time_h', 'time_m', 'data'
        ];
    }
}
