<?php
namespace app\models;

class Events extends \yii\mongodb\ActiveRecord {
    public function rules() {
        return [
            [['_id', 'type', 'time', 'status'], 'safe'],
        ];
    }

    public static function cancel($type) {
        $events = self::find()->where(['status' => 'new'])->all();
        foreach($events as $event) {
            if(stristr($event->type, $type)) {
                $event->status = 'old';
                $event->save();
            }
        }
    }

    public static function getNew() {
        $new_events = [];
        $events = self::find()->where(['status' => 'new'])->select(['type', '_id'])->asArray()->all();
        foreach($events as $event) {
            $new_events[(string)$event['_id']] = $event['type'];
        }
        return $new_events;
    }

    public static function addEvent($type) {
        $e = new self;
        $e->type = $type;
        $e->status = 'new';
        $e->time = microtime(true);
        $e->save();
    }

    public static function collectionName() {
        return ['wmbot', 'events'];
    }

    public function attributes() {
        return ['_id', 'type', 'time', 'status'];
    }

    public function attributeLabels() {
        return [
            '_id' => '0',
            'type', 'time', 'status'
        ];
    }
}
